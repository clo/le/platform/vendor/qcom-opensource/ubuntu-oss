#!/usr/bin/env python
#
# Copyright (c) 2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# This script will help to create update-alternatives files
# for all QC packages automaticlly.
#
# OE packages information:
#     dic1 = { runtime_pkg:[recipe_pkg bb_name /file1_path /file2_path ...], ... }
# OSS package information which just contains conflict file:
#     dic2 = { runtime_pkg:[oss_pkg: /file1_path /file2_path ...], ... }
# OSS package information which contains conlict file's correspoding package's all files 
#     dic3 = { oss_pkg:[/file1_path /file2_path ...], ... }
# OSS package information which contains conlict pkg's all files, now runtime_pkg = oss_pkg. 
#     dic4 = { oss_pkg:[/file1_path /file2_path ...], ... }

import sys, os
import shutil
from time import *
import argparse
parser = argparse.ArgumentParser(description='create update-alternatives files')
parser.add_argument('--pkg', type=str, default = None, help='input pkg name which defined in your bb file')
args = parser.parse_args()

status_path = "./tmp-glibc/work/qrb5165_rb5-oe-linux/qti-ubuntu-robotics-image/1.0-r0/rootfs/var/lib/dpkg/status"
oe_pkg_names = "update-alternatives/config/log/1_oe_pkg_names.txt"
oe_file_list = "update-alternatives/config/log/2_oe_file.txt"
search_file_list = "update-alternatives/config/log/3_search_file.txt"
filter_file_list = "update-alternatives/rootfs/4_filter_file.txt"
filter_file_list_full = "update-alternatives/rootfs/5_filter_file_full.txt"
filter_pkg_list = "update-alternatives/rootfs/6_filter_pkg.txt"
get_files_shell = "./get_files.sh"
info_magic = "Info_Magic:"

def get_oe_from_status(filename):
    if not os.path.exists(filename):
        print("Error: %s do not exit!\nPlease run: bitbake qti-ubuntu-robotics-image" % filename)
        exit()
    f = open(filename, 'r')
    str1 = f.read()
    lst = str1.split('\n')
    i = 0
    m = 0
    pkg_names = ""
        
    while i < len(lst):
          if lst[i].find("OE: ") != -1:
             m = i + 1
             while i > 0:
                     i -= 1
                     if lst[i].find("Package: ") != -1:
                        pkg_names = pkg_names + lst[i].split()[1] + '\n'
                        i = m
                        break
          else:
               i += 1
    with open(oe_pkg_names, 'w') as file_object:
         file_object.write(pkg_names)

    ret = os.system(get_files_shell + " " + "oe" + " " + oe_pkg_names)
    if ret != 0:
        print("Error: could not get oe packages from status!")
        exit()

def get_oe_from_input(pkgs):
    pkg_names = ""
    for lst in pkgs.split(','):
        if lst not in pkg_names.split():
            pkg_names = pkg_names + lst + '\n'
    f = open(oe_pkg_names, 'w')
    f.write(pkg_names)
    f.close()

    ret = os.system(get_files_shell + " " + "runtime_pkgs" + " " + oe_pkg_names)
    if ret != 0:
        print("please input correct pkg name which defined in your bb file!")
        exit()
    ret = os.system(get_files_shell + " " + "oe" + " " + oe_pkg_names)
    if ret != 0:
        print("Error: could not get oe packages from input!")
        exit()

def get_oe_pkgs():
    if args.pkg:
        get_oe_from_input(args.pkg)
    else:
        get_oe_from_status(status_path)

def read_oe_pkgs(filename):
    f = open(filename, 'r')
    str1 = f.read()
    lst = str1.strip().split('\n')
    i = 0
    list1 = []
    dic = {}
       
    while i < len(lst):
          if lst[i].split()[0] == info_magic:
             runtime_pkg = lst[i].split()[1]
             list1 = []
             list1.append(lst[i].split()[2])
             list1.append(lst[i].split()[3])
             dic1 = {runtime_pkg:list1}
             dic.update(dic1)
          else:
              if lst[i][0] == '\t':
                 dic[runtime_pkg].append(lst[i][1:])
          i += 1
    return dic 

def get_oss_pkgs(dic):
    file_names = ""

    for key in dic:
        if len(dic[key]) > 2:
            list1 = dic[key][2:]
            for f in list1:
                file_names = file_names + f + '\n'
    with open(search_file_list, 'w') as file_object:
         file_object.write(file_names)
    ret = os.system(get_files_shell + " " + "ubuntu")
    if ret != 0:
        print("Error: could not get oss packages from website!")
        exit()

def read_oss_pkg(filename):
    dic1 = {}
    pkg_name = ""

    f = open(filename, 'r')
    str1 = f.read()
    lst1 = str1.strip().split('\n')
    if not lst1[0]:
        return dic1
    for lst in lst1:
        if lst.split()[:-1] != pkg_name:
            pkg_name = lst.split()[0][:-1]
        if dic1.setdefault(pkg_name):
            dic1[pkg_name].append(lst.split()[1])
        else:
            temp_list = []
            temp_list.append(lst.split()[1])
            dic = {pkg_name:temp_list}
            dic1.update(dic)

    return dic1

def read_oss_pkgs(filter_file_list, filter_file_list_full, filter_pkg_list, dic1):
    dic2 = {}
    temp_list = []
    flag = False
    pkg_name = ""

    f1 = open(filter_file_list, 'r')
    str1 = f1.read()
    lst1 = str1.strip().split('\n')
    if lst1[0]:
        for lst in lst1:
            for key in dic1:
                if dic1[key] > 2:
                    list1 = dic1[key][2:]
                    for f in list1:
                        if lst.split()[1] == f:
                            if dic2.setdefault(key):
                                dic2[key].append(lst)
                            else:
                                temp_list = []
                                temp_list.append(lst)
                                dic = {key:temp_list}
                                dic2.update(dic)
                            flag = True
                            break
                    if flag == True:
                        flag = False
                        break

    dic3 = read_oss_pkg(filter_file_list_full)
    dic4 = read_oss_pkg(filter_pkg_list)

    return dic2, dic3, dic4

def write_pkg_info(head_info, file_name, oe_pkg, oe_list, oss_pkg, oss_list):
    str1 = ""
    str1_oe = ""
    str1_oss = ""
    if not os.path.isfile(file_name):
        for lst in oe_list:
            str1_oe += lst + '\n'
        str1 = "************%s**************\n" % head_info
        str1 += "OE package < %s >, file list:\n%s\n" % (oe_pkg, str1_oe)
        f1 = open(file_name, 'w')
    else:
        f1 = open(file_name, 'r+')
        old = f1.read()
        f1.seek(0)
        str1 = "************%s**************\n" % head_info
        f1.write(str1)
        f1.write(old)
        str1 = ""
    for lst in oss_list:
        str1_oss += lst + '\n'
    str1 += "OSS package < %s >, file list:\n%s\n" % (oss_pkg, str1_oss)
    f1.write(str1)
    f1.close()

def write_bbappend_alternatives(bb_name):
    bbappend_name = "update-alternatives/config/bbappend/" + bb_name + "_%.bbappend"
    if not os.path.isfile(bbappend_name):
        f1 = open(bbappend_name, 'w')
        str1 = "# config update-alternatives for %s\nrequire update-alternatives/%s.inc\n\n" % (bb_name, bb_name)
        f1.write(str1)
        f1.close()

def write_inc_alternatives(conf, list1, list2=None, dic3=None, count_list=None):
    inc_name = "update-alternatives/config/inc/%s.inc" % list1[1]
    if not os.path.isfile(inc_name):
        f2 = open(inc_name, 'w')
        str2 = "# use update-alternatives mechanism\ninherit update-alternatives-ubuntu\n\n"
        f2.write(str2)
        f2.close()
    alternative = " "
    f3 = open(inc_name, 'a')
    str3 = '# set update-alternatives symbolic link path and real target path for %s package\n' % list1[0]

    if conf == "conflict_file":
        #for lst in list2:
        lst_t = []
        for lst in list2:
            lst_t.append(lst.split()[1])
        lst_t = sorted(list(set(lst_t)))
        for lst in lst_t:
            alternative += os.path.basename(lst) + " "
        for lst in lst_t:
            str3 += 'ALTERNATIVE_LINK_NAME[%s] = "%s"\n\n' % (os.path.basename(lst), lst)
        oe_file = sorted(list1[2:])
        flag = False
        for lst in oe_file:
            for key in dic3:
                if lst in dic3[key]:
                    flag = True
            if os.path.basename(lst) in alternative.split():
                flag = True
            if flag == True:
                flag = False
                continue
            for key in dic3:
                for lst2 in dic3[key]:
                    if os.path.basename(lst) == os.path.basename(lst2):
                        alternative += os.path.basename(lst) + " "
                        str3 += 'ALTERNATIVE_LINK_NAME[%s] = "%s"\n' % (os.path.basename(lst), lst2)
                        str3 += 'ALTERNATIVE_TARGET_%s[%s] = "%s"\n\n' % (list1[0], os.path.basename(lst), lst)
                        flag = True
                if flag == True:
                    flag = False
                    break
    elif conf == "conflict_pkg":
        for lst in count_list:
            alternative += os.path.basename(lst.split()[0]) + " "
            str3 += 'ALTERNATIVE_LINK_NAME[%s] = "%s"\n' % (os.path.basename(lst.split()[0]), lst.split()[1])
            str3 += 'ALTERNATIVE_TARGET_%s[%s] = "%s"\n\n' % (list1[0], os.path.basename(lst.split()[0]), lst.split()[0])

    str4 = '# set update-alternatives priority for %s package\n' % list1[0]
    str4 += 'ALTERNATIVE_PRIORITY_%s = "100"\n\n' % list1[0]
    str4 += '# set update-alternatives register name for %s package\n' % list1[0]
    str4 += 'ALTERNATIVE_%s = "%s"\n\n' % (list1[0], alternative.strip())
    f3.write(str4)
    f3.write(str3)
    f3.close()

def write_alternatives(key, list1, list2, dic3):
    inc_name = "update-alternatives/config/inc/%s.inc" % list1[1]
    pkg_name = "update-alternatives/config/pkg/%s.pkg" % list1[1]

    # write pkg information
    conflict_pkg = ""
    for lst in list2:
        if not conflict_pkg == lst.split()[0]:
            conflict_pkg = lst.split()[0]
            head_info = "File Conflict < %s >" % conflict_pkg[:-1]
            write_pkg_info(head_info, pkg_name, key, list1[2:], conflict_pkg[:-1], dic3[conflict_pkg[:-1]])

    # write bbappend information
    write_bbappend_alternatives(list1[1])

    # write inc information
    write_inc_alternatives("conflict_file", list1, list2, dic3)


def write_pkg(key, list1, list4):
    pkg_name = "update-alternatives/config/pkg/%s.pkg" % list1[1]
    bbappend_name = "update-alternatives/config/bbappend/" + list1[1] + "_%.bbappend"

    # write pkg information
    head_info = "Pkg Conflict < %s >" % key
    write_pkg_info(head_info, pkg_name, key, list1[2:], key, list4)

    #count conflict file
    flist = list1[2:]
    count = ""
    count_list = []
    for lst in flist:
        for lst2 in list4:
            if os.path.basename(lst) == os.path.basename(lst2):
                count = lst + ' ' + lst2
                count_list.append(count)
                break
    if len(count_list) > 0:
        write_bbappend_alternatives(list1[1])
        write_inc_alternatives("conflict_pkg", list1, None, None, count_list)
    else:
        f1 = open(bbappend_name, 'a')
        str1 = 'PKG_%s = "%s"' % (list1[0], 'qti-' + list1[0])
        f1.write(str1)
        f1.close()

def write_ubuntubase():
    file_pkg = "update-alternatives/config/pkg"
    file_ubase = "update-alternatives/config/ubuntu-base"
    for root, dirs, files in os.walk(file_pkg, topdown=False):
        for name in files:
            pkgs = ""
            base_name = name[:-3] + 'base'
            f1 = open(os.path.join(root, name), 'r')
            str1 = f1.read()
            lst1 = str1.strip().split('\n')
            for lst in lst1:
                if "File Conflict" in lst or "Pkg Conflict" in lst:
                    ret = os.system(get_files_shell + " " + "check-install" + " " + lst.split()[3])
                    if ret == 0 and lst.split()[3] not in pkgs.split():
                        pkgs = pkgs + lst.split()[3] + '\n'
            f2 = open(os.path.join(file_ubase, base_name), 'w')
            f2.write(pkgs)

def analyze_show(dic1, dic2, dic4):
    file_list = []
    pkg_list = []
    file_conf = "update-alternatives/config"
    for root, dirs, files in os.walk(file_conf, topdown=False):
        for name in files:
            f1 = open(os.path.join(root, name), 'r')
            str1 = f1.read()
            lst1 = str1.strip().split()
            for lst in lst1:
                if "ALTERNATIVE_PRIORITY_" in lst:
                    file_list.append(lst[len("ALTERNATIVE_PRIORITY_"):])
                elif "PKG_" in lst:
                    pkg_list.append(lst[len("PKG_"):])

    if not file_list and not pkg_list:
        print("\n\n"
              "###################################\n"
              "########### No Conflict ###########\n"
              "###################################\n\n")
        return
    if file_list:
        print("\n\n"
              "###################################\n"
              "########## File Conflict ##########\n"
              "###################################\n%s\n" % file_list)
    if pkg_list:
        print("\n\n"
              "###################################\n"
              "######## Pkg Name Conflict ########\n"
              "###################################\n%s\n" % pkg_list)

def write_config(dic1, dic2, dic3, dic4):
    for key in dic1:
        if dic2.setdefault(key) == None and dic4.setdefault(key) == None:
            continue
        elif dic2.setdefault(key):
            write_alternatives(key, dic1[key], dic2[key], dic3)
        elif dic4.setdefault(key):
            write_pkg(key, dic1[key], dic4[key])
    write_ubuntubase()
    analyze_show(dic1, dic2, dic4)

def create_prop_folder():
    if os.path.lexists("update-alternatives/config/pkg"):
        shutil.rmtree("update-alternatives/config/pkg")
    if os.path.lexists("update-alternatives/config/bbappend"):
        shutil.rmtree("update-alternatives/config/bbappend")
    if os.path.lexists("update-alternatives/config/inc"):
        shutil.rmtree("update-alternatives/config/inc")
    if os.path.lexists("update-alternatives/config/ubuntu-base"):
        shutil.rmtree("update-alternatives/config/ubuntu-base")
    if not os.path.lexists("update-alternatives/config/log"):
        os.makedirs("update-alternatives/config/log")
    os.makedirs("update-alternatives/config/pkg")
    os.makedirs("update-alternatives/config/bbappend")
    os.makedirs("update-alternatives/config/inc")
    os.makedirs("update-alternatives/config/ubuntu-base")

if __name__ == '__main__':
    start = time()
    create_prop_folder()
    get_oe_pkgs()
    dic1 = read_oe_pkgs(oe_file_list)
    get_oss_pkgs(dic1)
    dic2, dic3, dic4 = read_oss_pkgs(filter_file_list, filter_file_list_full, filter_pkg_list, dic1)
    write_config(dic1, dic2, dic3, dic4)
    end = time()
    print('Success !!!, Running time: %s Seconds'%(end-start))
