#!/bin/bash
#
# Copyright (c) 2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function get_oe_file()
{
    file_list="update-alternatives/config/log/2_oe_file.txt"
    split="Info_Magic: "
    runtime_pkg=$(cat ${1})
    all_recipe_pkg=$(oe-pkgdata-util list-pkgs)
    all_recipe_pkg_arr=($all_recipe_pkg)
    all_runtime_pkg=$(oe-pkgdata-util lookup-pkg $all_recipe_pkg)
    all_runtime_pkg_arr=($all_runtime_pkg)
    i=0
    num_list=()
    info_list=""

    # search index
    for rpkg in $runtime_pkg;do
	    for apkg in $all_runtime_pkg;do
		    if [ $rpkg = $apkg ] ; then
			    num_list+=($i)
		    fi
		    let i++
	    done
	    i=0
    done

    # output all information to info_list
    # below it its format: 
    # <runtime_pkg recipe_pkg recipe_name>
    # <recipe_pkg:>
    # <file list .....>
    for((i=0;i<=${#num_list[@]}-1;i++))
    do
	files=$(oe-pkgdata-util list-pkg-files ${all_recipe_pkg_arr[${num_list[i]}]})
	recipe_name=`oe-pkgdata-util lookup-recipe ${all_recipe_pkg_arr[${num_list[i]}]} 2>/dev/null || oe-pkgdata-util lookup-recipe "qti-"${all_recipe_pkg_arr[${num_list[i]}]}`
	info_list="${info_list}${split}${all_runtime_pkg_arr[${num_list[i]}]} ${all_recipe_pkg_arr[${num_list[i]}]} ${recipe_name}\n${files}\n"
    done
    echo -e "$info_list" > "$file_list"
}

function get_ubuntu_file()
{
    ubuntu_tar_file=`pwd`/../downloads/ubuntu-base.done/ubuntu-base-18.04.5-base-arm64.tar.gz
    filter_file_list="4_filter_file.txt"
    filter_file_list_full="5_filter_file_full.txt"
    filter_pkg_list="6_filter_pkg.txt"

    if [ ! -f update-alternatives/rootfs/ubuntu_tar_file.done ];then
	rm update-alternatives/rootfs -rf
	mkdir update-alternatives/rootfs
        tar -xf $ubuntu_tar_file -C update-alternatives/rootfs
        touch update-alternatives/rootfs/ubuntu_tar_file.done
    fi
    cp update-alternatives/config/log/3_search_file.txt update-alternatives/rootfs/ -f
    cp update-alternatives/config/log/1_oe_pkg_names.txt update-alternatives/rootfs/ -f
    cd update-alternatives/rootfs
    fakechroot fakeroot chroot  . /bin/bash -c "apt-get update"
    fakechroot fakeroot chroot  . /bin/bash -c "apt-get install apt-file -y"
    fakechroot fakeroot chroot  . /bin/bash -c "apt-get update"
    fakechroot fakeroot chroot  . /bin/bash -c "apt-file search  -a arm64 -F -f 3_search_file.txt > $filter_file_list"
    fakechroot fakeroot chroot  . /bin/bash -c "apt-file list -a arm64 -f 1_oe_pkg_names.txt > $filter_pkg_list"

    pkgs=`cat $filter_file_list | awk '{print $1}' | sort | uniq | xargs`
    echo "" > temp.txt
    for pkg in $pkgs
    do
        echo ${pkg%?} >> temp.txt
    done
    fakechroot fakeroot chroot  . /bin/bash -c "apt-file list -a arm64 -f temp.txt > $filter_file_list_full"
    rm temp.txt -rf
    cp $filter_file_list $filter_pkg_list $filter_file_list_full ../config/log/ -f
}

function check_install()
{
    cd update-alternatives/rootfs
    fakechroot fakeroot chroot  . /bin/bash -c "dpkg -s $1 > temp.txt"
    if [[ -s temp.txt ]];then
	rm temp.txt -rf
	exit 1
    else
	rm temp.txt -rf
	exit 0
    fi
}

function get_runtime_pkgs()
{
    recipe_pkg=$(cat ${1})
    oe-pkgdata-util lookup-pkg $recipe_pkg > $1
}

case $1 in
    "oe")
        get_oe_file $2
    ;;
    "ubuntu")
	get_ubuntu_file
    ;;
    "check-install")
	check_install $2
    ;;
    "runtime_pkgs")
	get_runtime_pkgs $2
    ;;
esac

